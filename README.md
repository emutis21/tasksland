# Nombre del Proyecto

## Descripción

Este proyecto es una aplicación de tablero Kanban que permite a los usuarios organizar sus tareas en diferentes columnas (limbo, por hacer, haciendo y hecho). Los usuarios pueden crear, mover y eliminar tarjetas que representan tareas.

## Estructura del Proyecto

El proyecto tiene la siguiente estructura de directorios:

- `src/`: Contiene el código fuente de la aplicación.
- `public/`: Contiene los archivos estáticos de la aplicación.
- `prisma/`: Contiene los archivos de configuración de Prisma, incluyendo el esquema de la base de datos.
- `.next/`: Contiene los archivos generados por Next.js durante la compilación.
- `types/`: Contiene los archivos de definición de tipos TypeScript.

## Cómo ejecutar el proyecto

1. Instala las dependencias del proyecto con `pnpm install`.
2. Inicia el servidor de desarrollo con `pnpm dev`.
3. Abre [http://localhost:3000](http://localhost:3000) en tu navegador para ver la aplicación.

## Contribuir

Este proyecto es de código abierto y acepto contribuciones. Por favor, consulta las [guías de contribución](CONTRIBUTING.md) para más detalles.

## Licencia

Este proyecto está licenciado bajo los términos de la licencia [MIT](LICENSE).
